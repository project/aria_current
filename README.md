# aria-current

This module adds the aria-current='page' attribute to active links inside
navigation elements.

Currently no other aria-current versions are supported and only links inside
nav elements are targeted.


### Installing
Install like any Drupal module.
