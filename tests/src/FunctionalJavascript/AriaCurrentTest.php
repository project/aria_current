<?php

namespace Drupal\Tests\aria_current\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Tests the adding of aria-current attribute.
 *
 * @group aria_current
 */
class AriaCurrentTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['aria_current'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * Tests that the aria-current attribute is added to the homepage.
   */
  public function testAriaCurrent() {
    // Test for the logged in user.
    $account = $this->drupalCreateUser();
    $this->drupalLogin($account);
    $this->drupalGet('<front>');
    $link = $this->getSession()->getPage()->find('css', 'nav#block-stark-main-menu ul li a');
    $this->assertNotEmpty($link);
    $this->assertTrue($link->hasClass('is-active'));
    $this->assertTrue($link->hasAttribute('aria-current'));
    $this->assertEquals('page', $link->getAttribute('aria-current'));

    // Test for the anon user.
    $this->drupalLogout();
    $this->drupalGet('<front>');
    $link = $this->getSession()->getPage()->find('css', 'nav#block-stark-main-menu ul li a');
    $this->assertNotEmpty($link);
    $this->assertEquals('Home', $link->getText());
    $this->assertTrue($link->hasClass('is-active'));
    $this->assertTrue($link->hasAttribute('aria-current'));
    $this->assertEquals('page', $link->getAttribute('aria-current'));

    // Test that the attribute is not added when the link is not to the current
    // page.
    $this->drupalGet('user/login');
    $link = $this->getSession()->getPage()->find('css', 'nav#block-stark-main-menu ul li a');
    $this->assertNotEmpty($link);
    $this->assertEquals('Home', $link->getText());
    $this->assertFalse($link->hasClass('is-active'));
    $this->assertFalse($link->hasAttribute('aria-current'));
  }

}
